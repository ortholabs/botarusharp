﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using BotaruSharp.Source.Plugins;

namespace BotaruSharp
{
    public class Program
    {
        public static Settings settings = new Settings();
        private static string logPath = Environment.CurrentDirectory + "\\logs\\log" + DateTime.Now.Ticks + ".txt";

        public static TwitchIRC chatServer;
        public static TwitchIRC whisperServer;

        static void Main(string[] args)
        {
            if (!Directory.Exists(Path.GetDirectoryName(logPath))) { Directory.CreateDirectory(Path.GetDirectoryName(logPath)); }

            Log("Program", "Starting Botaru#", LogLevel.Debug);

            Log("Program", "Loading Settings", LogLevel.Debug);
            settings.Load();

            Log("Program", "Loading Plugins", LogLevel.Debug);
            PluginManager.Init();

            Log("Program", "Initializing IRC", LogLevel.Debug);
            chatServer = new TwitchIRC(settings.twitchAuth);
            whisperServer = new TwitchIRC(settings.whisperTwitchAuth){isWhisper = true};
            PluginManager.InitChatServer(chatServer);
            PluginManager.InitWhisperServer(whisperServer);

            while (whisperServer.connected)
            {
                chatServer.Poll();
                whisperServer.Poll();

                PluginManager.Update();

                Thread.Sleep(settings.pollInterval);
            }

            Log("Program", "Shutting Down", LogLevel.Debug);

            Console.ReadLine();
        }

        public static void Say(string message, string channel)
        {
            chatServer.Say(message, channel);
        }

        public static void Whisper(string username, string message)
        {
            whisperServer.Whisper(username, message);
        }

        public static void Log(string trace, string txt, LogLevel logLevel)
        {
            var log = false;

            switch (logLevel)
            {
                case LogLevel.Error: { if(settings.log.errors) log = true; } break;
                case LogLevel.Warning: { if (settings.log.warnings) log = true; } break;
                case LogLevel.Debug: { if (settings.log.debug) log = true; } break;
                default: { Console.WriteLine("Unhandled LogLevel " + logLevel + "; " + trace + ": " + txt); } break;
            }

            if (log)
            {
                var logTxt = logLevel + " - " + trace + ": " + txt;
                Console.WriteLine(logTxt);

                if (settings.log.saveToFile)
                {
                    File.AppendAllText(logPath, logTxt + Environment.NewLine);
                }
            }
        }

        public enum LogLevel
        {
            Error,
            Warning,
            Debug
        }
    }
}
