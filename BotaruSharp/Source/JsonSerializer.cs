﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace BotaruSharp
{
    public static class JsonSerializer
    {
        public static bool indented = true;

        public static void SerializeToFile(string path, object objectToSerialize)
        {
#if !DEBUG
            try
            {
#endif
            if (!Directory.Exists(path)) Directory.CreateDirectory(Path.GetDirectoryName(path));

            var data = Serialize(objectToSerialize);

            if (!string.IsNullOrWhiteSpace(data))
                File.WriteAllText(path, data);
#if !DEBUG
            } catch{}
#endif
        }

        public static string Serialize(object objectToSerialize)
        {
#if !DEBUG
            try
            {
#endif
            return JsonConvert.SerializeObject(objectToSerialize, indented ? Formatting.Indented : Formatting.None);
#if !DEBUG
            } catch{}

            return null;
#endif
        }

        public static void DeserializeFromFile(string path, object objectToPopulate)
        {
#if !DEBUG
            if (!File.Exists(path)) return;
#endif

#if !DEBUG
            try
            {
#endif
            JsonConvert.PopulateObject(File.ReadAllText(path), objectToPopulate);
#if !DEBUG
            } catch{}
#endif
        }

        public static void Deserialize(string json, object objectToPopulate)
        {
#if !DEBUG
            try
            {
#endif
            JsonConvert.PopulateObject(json, objectToPopulate);
#if !DEBUG
            } catch{}
#endif
        }
    }
}
