﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace BotaruSharp
{
    [Serializable]
    public class Settings
    {
        [NonSerialized] public string settingsPath = Environment.CurrentDirectory + "\\config.txt";

        public TimeSpan pollInterval = new TimeSpan(0, 00, 00, 01, 0000);

        public TwitchAuth twitchAuth = new TwitchAuth();
        [NonSerialized] public TwitchAuth whisperTwitchAuth = new TwitchAuth();
        public WhisperAuth whisperAuth = new WhisperAuth() { server = "199.9.253.119", port = 6667 };
        public Log log = new Log();

        public class WhisperAuth
        {
            public string server = "irc.twitch.tv";
            public int port = 6667;
        }

        public class TwitchAuth : ICloneable
        {
            public object Clone()
            {
                return MemberwiseClone();
            }

            public List<string> channels = new List<string>();
            public string username = "justinfan6667";
            public string oauth = "";

            public string server = "irc.twitch.tv";
            public int port = 6667;

            //TODO: Automate this? Keep track of if the bot is a mod in every channel, and auto-adjust if this flag is true
            /// <summary>
            /// If the bot is a mod in every channel, it can send messages at a faster rate of 100/30s instead of 20/30s.
            /// If this flag is set, and the bot ISN'T a mod in every channel, the bot has the chance of being banned for 8 hours.
            /// If unsure, keep this flag set to false.
            /// </summary>
            public bool isModInEveryChannel = false;

            /// <summary>
            /// Set to true if the username is set to justinfan.
            /// </summary>
            [NonSerialized] public bool canTalk = false;
        }

        public class Log
        {
            public bool errors = true;
            public bool warnings = false;
            public bool debug = false;
            public bool saveToFile = true;
        }

        public void Load()
        {
            try
            {
                if (File.Exists(settingsPath))
                {
                    JsonSerializer.DeserializeFromFile(settingsPath, this);
                    Program.Log("Settings", "Loaded settings from file.", Program.LogLevel.Debug);
                }
                else
                {
                    Program.Log("Settings", "Using default config.", Program.LogLevel.Warning);
                }

                whisperTwitchAuth = (TwitchAuth)twitchAuth.Clone();
                whisperTwitchAuth.channels = new List<string>();
                whisperTwitchAuth.port = whisperAuth.port;
                whisperTwitchAuth.server = whisperAuth.server;

                //ensures all the variables in the config are up to date, or that the file exists at all
                Save();
            }
            catch (Exception e)
            {
                Program.Log("Settings", "Load Error - " + e.Message, Program.LogLevel.Error);
            }
        }

        public void Save()
        {
            try
            {
                JsonSerializer.SerializeToFile(settingsPath, this);
            }
            catch (Exception e)
            {
                Program.Log("Settings", "Save Error - " + e.Message, Program.LogLevel.Error);
            }
        }
    }
}
