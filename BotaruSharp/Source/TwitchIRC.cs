﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace BotaruSharp
{
    public class TwitchIRC
    {
        //TODO: Write lockout protection, 20 messages within 30 seconds
        //If the user is ONLY sending messages to channels where it has mod/op, this can be raised to 100 messages

        public event EventHandler<RawMessageArgs> rawMessage;
        public event EventHandler<ChatMessageArgs> chatMessage;

        TcpClient IRCConnection = null;
        NetworkStream ns = null;
        StreamReader sr = null;
        StreamWriter sw = null;

        public bool connected = false;
        public bool isWhisper = false;
        private Settings.TwitchAuth auth;

        private List<string> storedData = new List<string>();
        private Thread workThread;

        public TwitchIRC(Settings.TwitchAuth auth)
        {
            this.auth = auth;

            try
            {
                IRCConnection = new TcpClient(auth.server, auth.port);
            }
            catch (Exception e)
            {
                Program.Log("TwitchIRC", "Connection to " + auth.server + " failed.", Program.LogLevel.Error);
                return;
            }


            if (auth.channels.Count == 0)
            {
                Program.Log("TwitchIRC", "No channels set to connect to.", Program.LogLevel.Warning);
            }

            ns = IRCConnection.GetStream();
            sr = new StreamReader(ns);
            sw = new StreamWriter(ns);

            auth.username = auth.username.ToLower();

            if (auth.oauth.StartsWith("oauth:"))
                auth.oauth = auth.oauth.Substring("oauth:".Length);

            if (!auth.username.StartsWith("justinfan"))
                auth.canTalk = true;

            SendData("CAP REQ :twitch.tv/tags twitch.tv/commands twitch.tv/membership");
            if(auth.canTalk) SendData("PASS oauth:" + auth.oauth);
            SendData("NICK " + auth.username);

            for (var i = 0; i < auth.channels.Count; i++)
            {
                auth.channels[i] = auth.channels[i].ToLower();
                SendData("JOIN #" + auth.channels[i]);
            }

            connected = true;

            workThread = new Thread(WorkThread);
            workThread.Start();
        }

        private void HandleDisplayingData(string s)
        {
            var ircMessage = new IrcMessage(s);
            if(rawMessage != null) rawMessage(this, new RawMessageArgs(s));

            if (ircMessage.Command.Equals("PING"))
            {
                SendData("PONG " + ircMessage.Parameters[0], null);
            }
            else if (ircMessage.Command.Equals("PRIVMSG"))
            {
                var msg = new ChatMessage();
                msg.name = "";
                msg.isMod = false;
                msg.isBroadcaster = false;
                msg.isSubscriber = false;
                msg.msg = ircMessage;
                msg.channel = ircMessage.Parameters[0].Substring(1);

                foreach (var keypair in ircMessage.TwitchParams)
                {
                    switch (keypair.Key)
                    {
                        case "display-name":
                        {
                            msg.name = keypair.Value;
                        } break;
                        case "user-type":
                        {
                            if (keypair.Value.Equals("mod"))
                                msg.isMod = true;
                        } break;
                        case "subscriber":
                        {
                            if (keypair.Value.Equals("1"))
                                msg.isSubscriber = true;
                        } break;
                    }
                }

                if (string.IsNullOrWhiteSpace(msg.name))
                {
                    msg.name = ircMessage.Prefix.Substring(1).Split('!')[0];
                    msg.name = msg.name.ToUpper()[0] + msg.name.Substring(1).ToLower();
                }

                if (!string.IsNullOrWhiteSpace(msg.name) && msg.msg.Parameters.Length > 1 && !string.IsNullOrWhiteSpace(msg.msg.Parameters[1]) && !string.IsNullOrWhiteSpace(msg.channel))
                {
                    var text = msg.msg.Parameters[1];
                    if (text.Length > 0 && text[0] == 1 && text.Substring(1).StartsWith("ACTION") && text.Last() == 1)
                    {
                        text = text.Substring(7, text.Length - 8);
                        msg.msg.Parameters[1] = text;
                        msg.msgIsEmote = true;
                    }

                    if (msg.name.ToLower().Equals(msg.channel.ToLower()))
                    {
                        msg.isMod = true;
                        msg.isBroadcaster = true;
                    }

                    msg.msg.Parameters[1] = msg.msg.Parameters[1].Trim();

                    //this is just an event for signalling a chat message was said, you could just handle commands here or send the msg off to another function to parse the commands there if any
                    if (chatMessage != null) chatMessage(this, new ChatMessageArgs(msg));

                    //this could just be a Console Writeline
                    Program.Log("TwitchIRC", msg.name + (msg.isMod ? (msg.isBroadcaster ? " the broadcaster" : " the mod") : (msg.isSubscriber ? " the subscriber" : "")) + (msg.msgIsEmote ? " emotes: " : " says: ") + msg.msg.Parameters[1], Program.LogLevel.Debug);
                }
            }
            else
            {
                Program.Log("TwitchIRC", "Unhandled " + s, Program.LogLevel.Warning);
            }
        }

        void WorkThread()
        {
            try
            {
                while (connected)
                {
                    string data;
                    while ((data = sr.ReadLine()) != null)
                    {
                        lock (storedData)
                        {
                            storedData.Add(data);
                        }
                    }
                }

                sr.Close();
            }
            catch (Exception e)
            {
                Program.Log("TwitchIRC", "WorkThread failed.", Program.LogLevel.Error);
            }
        }

        public void Poll()
        {
            if (IRCConnection != null && !IRCConnection.Connected)
            {
                Cleanup();
                return;
            }

            lock (storedData)
            {
                foreach (var data in storedData)
                {
                    HandleDisplayingData(data);
                }

                storedData.Clear();
            }

            //handle all saved up data
        }

        public string DefaultChannel()
        {
            if (auth.channels.Count > 0)
            {
                return auth.channels[0];
            }

            return null;
        }

        public void Say(string message, string channel = null)
        {
            if (string.IsNullOrWhiteSpace(channel)) channel = DefaultChannel();

            if (string.IsNullOrWhiteSpace(channel))
            {
                Program.Log("TwitchIRC", "Trying to talk to a null channel.", Program.LogLevel.Error);
                return;
            }

            if (!auth.channels.Contains(channel.ToLower()))
            {
                Program.Log("TwitchIRC", "Not connected to the " + channel + " channel.", Program.LogLevel.Error);
                return;
            }

            if (!auth.canTalk)
            {
                Program.Log("TwitchIRC", "Bot doesn't have talk privilege.", Program.LogLevel.Warning);
            }

            if (!channel.Trim().StartsWith("#")) channel = "#" + channel;
            SendData("PRIVMSG ", channel + " : " + message);
        }

        public void Whisper(string username, string message)
        {
            if(auth.channels.Count == 0)
                Say("/w " + username.ToLower() + " " + message, "#jtv");
            else
                Program.Log("TwitchIRC", "Cannot whisper when connected to channels.", Program.LogLevel.Warning);
        }

        public void JoinChannel(string channel)
        {
            if (!auth.channels.Contains(channel.ToLower()))
            {
                auth.channels.Add(channel.ToLower());
                SendData("JOIN #" + channel);
            }
        }

        public void LeaveChannel(string channel)
        {
            if (auth.channels.Contains(channel.ToLower()))
            {
                auth.channels.Remove(channel.ToLower());
                SendData("PART #" + channel);
            }
        }

        public List<string> GetChannels()
        {
            return auth.channels.ToList();
        }

        public void SendData(string cmd, string param = null)
        {
            var send = cmd + (param == null || param.Trim().Equals("") ? "" : " " + param);
            sw.WriteLine(send);
            sw.Flush();
            if(!send.Contains("PASS oauth"))
                Program.Log("TwitchIRC", "Sending: " + send, Program.LogLevel.Debug);
            else
                Program.Log("TwitchIRC", "Sending: Password", Program.LogLevel.Debug);
        }

        public void Cleanup()
        {
            connected = false;

            if (sw != null)
                sw.Close();
            if (ns != null)
                ns.Close();
            if (IRCConnection != null)
                IRCConnection.Close();
        }

        public struct ChatMessage
        {
            public string name;
            public bool isMod;
            public bool isBroadcaster;
            public bool isSubscriber;
            public IrcMessage msg;
            public bool msgIsEmote;
            public string channel;
        }

        public class IrcMessage
        {
            public string RawMessage { get; private set; }
            public string Prefix { get; private set; }
            public string Command { get; private set; }
            public string[] Parameters { get; private set; }

            public Dictionary<string, string> TwitchParams { get; private set; }

            public IrcMessage(string rawMessage)
            {
                RawMessage = rawMessage;

                if (rawMessage.StartsWith("@"))
                {
                    TwitchParams = new Dictionary<string, string>();
                    var splitParams = rawMessage.Substring(1, rawMessage.IndexOf(' ') - 1).Split(';');
                    rawMessage = rawMessage.Substring(rawMessage.IndexOf(' ') + 1);

                    foreach (var splitParam in splitParams)
                    {
                        var keyPair = splitParam.Split('=');
                        var key = keyPair[0];
                        var value = keyPair[1];

                        TwitchParams.Add(key, value);
                    }
                }

                if (rawMessage.StartsWith(":"))
                {
                    Prefix = rawMessage.Substring(1, rawMessage.IndexOf(' ') - 1);
                    rawMessage = rawMessage.Substring(rawMessage.IndexOf(' ') + 1);
                }

                if (rawMessage.Contains(' '))
                {
                    Command = rawMessage.Remove(rawMessage.IndexOf(' '));
                    rawMessage = rawMessage.Substring(rawMessage.IndexOf(' ') + 1);

                    var param = new List<string>();
                    while (!string.IsNullOrEmpty(rawMessage))
                    {
                        if (rawMessage.StartsWith(":"))
                        {
                            param.Add(rawMessage.Substring(1));
                            break;
                        }
                        if (!rawMessage.Contains(' '))
                        {
                            param.Add(rawMessage);
                            rawMessage = string.Empty;
                            break;
                        }

                        param.Add(rawMessage.Remove(rawMessage.IndexOf(' ')));
                        rawMessage = rawMessage.Substring(rawMessage.IndexOf(' ') + 1);
                    }

                    Parameters = param.ToArray();
                }
                else
                {
                    //Violates RFC 1459, but are possible cases for it for Twitch
                    Command = rawMessage;
                    Parameters = new string[]{};
                }
            }
        }

        public class RawMessageArgs : EventArgs
        {
            public RawMessageArgs(string message) { this.message = message; }

            public string message;
        }

        public class ChatMessageArgs : EventArgs
        {
            public ChatMessageArgs(ChatMessage message) { this.message = message; }

            public ChatMessage message;
        }
    }
}