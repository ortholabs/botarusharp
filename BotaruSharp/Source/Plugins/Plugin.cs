﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BotaruSharp
{
    public abstract class Plugin
    {
        public virtual void InitChatServer(TwitchIRC chatServer){}
        public virtual void InitWhisperServer(TwitchIRC whisperServer){}
        public virtual void Init(Settings settings){}

        /// <summary>
        /// Update happens at Settings.PollInterval, after the chat and whisper servers have been polled
        /// </summary>
        public virtual void Update(){}
    }
}
