﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BotaruSharp.Source.Plugins
{
    public static class PluginManager
    {
        static List<Plugin> plugins = new List<Plugin>();

        public static void SetupPluginOrder()
        {
            //plugins.Add(new BotaruPlugin());
        }

        public static void Init()
        {
            SetupPluginOrder();

            foreach (var plugin in plugins)
            {
                plugin.Init(Program.settings);
            }
        }

        public static void InitChatServer(TwitchIRC chatServer)
        {
            foreach (var plugin in plugins)
            {
                plugin.InitChatServer(chatServer);
            }
        }

        public static void InitWhisperServer(TwitchIRC whisperServer)
        {
            foreach (var plugin in plugins)
            {
                plugin.InitWhisperServer(whisperServer);
            }
        }

        public static void Update()
        {
            foreach (var plugin in plugins)
            {
                plugin.Update();
            }
        }
    }
}
